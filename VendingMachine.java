import java.net.*;
import java.util.*;
import java.io.*;

public class VendingMachine extends Thread {

	public static void main(String[] args) throws Exception {

		if (args.length > 1) {
			Client client = new Client(args[0], Integer.parseInt(args[1]));
			client.run();
		} else {
			Server server = new Server(Integer.parseInt(args[0]));
			server.run();
		}
	}
}

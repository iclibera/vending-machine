import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Server {
	private static final String DB_FILE = "item_list.txt";
	
	private final int port;
	
	private Map<Integer, Integer> amountMap = new HashMap<Integer, Integer>();
	private Map<Integer, String> nameMap = new HashMap<Integer, String>();
	
	public Server(int port) throws Exception {
		this.port = port;
		this.readDatabase();
	}
	
	private void readDatabase() throws Exception {
		BufferedReader br = null;
		String[] aLine;
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(DB_FILE));

			while ((sCurrentLine = br.readLine()) != null) {
				aLine = sCurrentLine.split(" ");
				amountMap.put(Integer.parseInt(aLine[0]), Integer.parseInt(aLine[2]));
				nameMap.put(Integer.parseInt(aLine[0]), aLine[1]);
			}
			System.out.println("a line is added here");
			System.out.println(DB_FILE + " is read");
		} finally {
			if (br != null)br.close();
		}
	}
	
	public void run() throws IOException {
		ServerSocket serverSocket = new ServerSocket(this.port);
		while (true) {
			System.out.println("The current list of items:");
			for (Integer key: amountMap.keySet()) {
				System.out.println("\t" + key + " " + nameMap.get(key) + " " + amountMap.get(key));
			}
			System.out.print("\nWaiting for a client... ");
			Socket socket = serverSocket.accept();
			System.out.println(" A client is connected.");
			try {
				PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					System.out.println("The received message:");
					System.out.println("\t" + inputLine);
					if (inputLine.equals("GET ITEM LIST")) {
						in.readLine();
						System.out.println("Send the message:");
						out.write("ITEM LIST\r\n");
						System.out.println("\tITEM LIST");
						for(Integer key: amountMap.keySet()) {
							String ll = key + " " + nameMap.get(key) + " " + amountMap.get(key) + "\r\n";
							out.write(ll);
							System.out.print("\t" + ll);
						}
						out.write("\r\n");
						out.flush();
					} else if (inputLine.equals("GET ITEM")) {
						List<Integer> items = new ArrayList<Integer>();
						List<Integer> amounts = new ArrayList<Integer>();
						while ((inputLine = in.readLine()).length() > 0) {
							System.out.println("\t" + inputLine);
							String[] str = inputLine.split(" ");
							items.add(Integer.parseInt(str[0]));
							amounts.add(Integer.parseInt(str[1]));
						}
						boolean valid = true;
						for (int i = 0; i< items.size(); i++) {
							if (amountMap.get(items.get(i)) < amounts.get(i)) {
								valid = false;
								break;
							}
						}
						if (!valid) {
							System.out.println("Send the message:");
							String ll = "OUT OF STOCK\r\n";
							out.write(ll);
							out.write("\r\n");
							System.out.print("\t" + ll);
						} else {
							for (int i = 0; i< items.size(); i++) {
								amountMap.put(items.get(i), amountMap.get(items.get(i)) - amounts.get(i));
							}
							System.out.println("Send the message:");
							String ll = "SUCCESS\r\n";
							out.write(ll);
							out.write("\r\n");
							System.out.print("\t" + ll);
						}
						out.flush();
					}
				}
			} catch(IOException e) {
				System.out.println("The client has terminated the connection.");
			}
		}
	}
	
}

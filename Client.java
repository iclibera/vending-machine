import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Client {
	private final String host;
	private final int port;
	
	private Map<Integer, Integer> summary = new HashMap<Integer, Integer>();
	
	public Client(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public void run() throws UnknownHostException, IOException {
		Socket socket = new Socket(host, port);
		PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
	    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	    
	    System.out.println("The connection is established.");
	    Scanner scanner = new Scanner(System.in);
	    String ret;
	    while (true) {
	    	System.out.print("Choose a message type (GET ITEM (L)IST, (G)ET ITEM, (Q)UIT: ");
	    	ret = scanner.next();
	    	if (ret.equals("L")) {
	    		out.write("GET ITEM LIST\r\n\r\n");
	    		out.flush();
	    		String line = in.readLine();;
	    		System.out.println(line);
	    		while((line = in.readLine()).length() > 0) {
	    			System.out.println("\t" + line);
	    		}
	    	} else if (ret.equals("G")) {
	    		System.out.print("Give the item id: ");
	    		int itemid = Integer.parseInt(scanner.next());
	    		System.out.print("Give the number of items: ");
	    		int amount = Integer.parseInt(scanner.next());
	    		out.write("A PROBLEM OCCURED\r\n");
			//out.write("GET ITEM\r\n");
	    		out.write(itemid + " " + amount + "\r\n\r\n");
	    		//out.flush();
	    		//System.out.println("The received message:");
	    		//String result = in.readLine();
	    		//System.out.println(result);
	    		if (result.equals("SUCCESS")) {
	    			Integer previous = summary.get(itemid);
	    			if (previous == null)
	    				previous = 0;
	    			summary.put(itemid, previous + amount);
	    		}
	    	} else if (ret.equals("Q")) {
	    		System.out.println("The summary of received items:");
	    		for (Integer key: summary.keySet()) {
	    			System.out.println("\t" + key + " " + summary.get(key));
	    		}
	    		socket.close();
	    	}
	    }
	}
}
